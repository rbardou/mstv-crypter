//
//  main.m
//  mstv-crypter
//
//  Created by Remy Bardou on 20/05/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EncryptionManager.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        void(^printUsage)() = ^{
            printf("Usage: mstv-crypter -i inputfile -o outputfile -s your_secret_key -m (encode|decode) [-p]\n");
            printf("\n");
            printf("-i \t--input\tinput file (mandatory)\n");
            printf("-o \t--output\toutput file (mandatory)\n");
            printf("-s \t--secret\tyour AES256 key (mandatory)\n");
            printf("-m \t--mode\tencode or decode to encrypt or decrypt the input file\n");
            printf("-p \t\t\tprints out a code snippet to 'securely' embed your key in an app.\n");
            printf("\nCopyright (c) 2015 MyStudioFactory. All rights reserved.\n");
        };
        
        if (argc == 1) {
            printUsage();
            return 0;
        }
        
        NSUserDefaults *args = [NSUserDefaults standardUserDefaults];
        
        NSString *inputFile = ([args stringForKey:@"i"] ? [args stringForKey:@"i"] : [args stringForKey:@"-input"]);
        NSString *outputFile = ([args stringForKey:@"o"] ? [args stringForKey:@"o"] : [args stringForKey:@"-output"]);
        
        NSString *key = ([args stringForKey:@"s"] ? [args stringForKey:@"s"] : [args stringForKey:@"-secret"]);
        
        NSString *mode = [([args stringForKey:@"m"] ? [args stringForKey:@"m"] : [args stringForKey:@"-mode"]) lowercaseString];
        
        
        printf("AppPath %s\n", argv[0]);
        
        if (key.length == 0) {
            printf("No key specified!\n");
            printUsage();
            return 0;
        } else if ([[NSProcessInfo processInfo].arguments containsObject:@"-p"]) {
            
            printf("Use the following snippet of code to get your secret key from your app code (Objective-C):\n\n");
            
            NSMutableString *oddString = [[NSMutableString alloc] initWithString:@"char odd[] = {"];
            NSMutableString *evenString = [[NSMutableString alloc] initWithString:@"char even[] = {"];
            
            NSInteger count = key.length;
            for (int i = 0; i < count; i++) {
                NSString *letter = [key substringWithRange:NSMakeRange(i, 1)];
                
                if (i % 2 == 0) {
                    if (i > 1) [oddString appendString:@", "];
                    
                    [oddString appendFormat:@"'%@'", letter];
                } else {
                    if (i > 1) [evenString appendString:@", "];
                    
                    [evenString appendFormat:@"'%@'", letter];
                }
            }
            [oddString appendString:@"};\n"];
            [evenString appendString:@"};\n"];
            
            NSMutableString *snippet = [[NSMutableString alloc] init];
            [snippet appendString:@"- (NSString *) mySecretKey {\n"];
            [snippet appendFormat:@"\t%@", oddString];
            [snippet appendFormat:@"\t%@", evenString];
            [snippet appendString:@"\tint arraySize = sizeof(odd);\n"];
            [snippet appendString:@"\tNSMutableString *key = [[NSMutableString alloc] init];\n"];
            [snippet appendString:@"\tfor (int i = 0; i < arraySize; i++) {\n"];
            [snippet appendString:@"\t\t[key appendFormat:@\"%c%c\", odd[i], even[i]];\n"];
            [snippet appendString:@"\t}\n"];
            [snippet appendString:@"\treturn key;\n"];
            [snippet appendString:@"}"];
            
            printf("%s", [snippet UTF8String]);
            
        } else if (inputFile.length == 0) {
            printf("No input file specified!\n");
            printUsage();
            return 0;
        } else if (outputFile.length == 0) {
            printf("No output file specified!\n");
            printUsage();
            return 0;
        } else if ([mode isEqualToString:@"encode"]) {
            printf("Encrypting %s into %s\n", [inputFile UTF8String], [outputFile UTF8String]);
            [EncryptionManager encryptFile:inputFile into:outputFile usingSecret:key];
            return 1;
        } else if ([mode isEqualToString:@"decode"]) {
            printf("Decrypting %s into %s\n", [inputFile UTF8String], [outputFile UTF8String]);
            [EncryptionManager decryptFile:inputFile into:outputFile usingSecret:key];
            return 1;
        } else {
            printf("Unknown mode requested!\n");
            printUsage();
        }
    }
    return 0;
}
