//
//  EncryptionManager.h
//  mstv-crypter
//
//  Created by Remy Bardou on 26/05/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EncryptionManager : NSObject

+ (BOOL) encryptFile:(NSString *)clearFile into:(NSString *)encryptedFile usingSecret:(NSString *)secret;
+ (BOOL) decryptFile:(NSString *)encryptedFile into:(NSString *)clearFile usingSecret:(NSString *)secret;

@end
