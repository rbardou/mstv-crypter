//
//  NSData+Encryption.h
//  ingdirect
//
//  Created by Remy Bardou on 20/05/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Encryption)

- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;

@end
