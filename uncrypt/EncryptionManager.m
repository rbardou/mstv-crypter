//
//  EncryptionManager.m
//  mstv-crypter
//
//  Created by Remy Bardou on 26/05/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import "EncryptionManager.h"
#import "NSData+Encryption.h"

@implementation EncryptionManager

+ (BOOL) encryptFile:(NSString *)clearFile
                into:(NSString *)encryptedFile
         usingSecret:(NSString *)secret {
    
    
    NSData *baseData    = [NSData dataWithContentsOfFile:clearFile];

    NSData *encData     = [baseData AES256EncryptWithKey:secret];

    // check if the data is decryptable
    NSData *decData     = [encData AES256DecryptWithKey:secret];

    if ([baseData isEqualToData:decData]) {
        [encData writeToFile:encryptedFile atomically:YES];
        return YES;
    } else {
        return NO;
    }
}

+ (BOOL) decryptFile:(NSString *)encryptedFile
                into:(NSString *)clearFile
         usingSecret:(NSString *)secret {
    
    NSData *baseData    = [NSData dataWithContentsOfFile:encryptedFile];
    
    NSData *decData     = [baseData AES256DecryptWithKey:secret];
    
    // check if the data is decryptable
    NSData *encData     = [decData AES256EncryptWithKey:secret];
    
    if ([baseData isEqualToData:encData]) {
        [decData writeToFile:clearFile atomically:YES];
        return YES;
    } else {
        return NO;
    }
}

@end
